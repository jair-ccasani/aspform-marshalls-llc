﻿using Prueba.Marshalls.DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace Prueba.Marshalls.BusinessLogic
{
    public class EmployeeBL
    {

        private readonly BDTestEntities _context;
        public EmployeeBL()
        {
            _context = new BDTestEntities();
        }
        public List<string> FilterEmployeeCodeAsync(string employeeCode)
        {
            var employeeCodes =  _context.tblEmployee.Where(x => x.Employee_Code.ToLower().Contains(employeeCode.Trim().ToLower())).Select(x => x.Employee_Code).Distinct().Take(10).ToList();
            return employeeCodes?.ToList();
        }

        public List<tblEmployee> ListEmployeeByCodeAsync(string employeeCode)
        {
            var employeeCodes =  _context.tblEmployee.Where(x => x.Employee_Code.ToLower().Equals(employeeCode.Trim().ToLower())).OrderByDescending(x=>x.Year).ThenByDescending(x=> x.Month).ToList();
            return employeeCodes?.ToList();
        }
    }
}
