﻿<%@ Page Title="Employee" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Prueba.Marshalls.WebForm.Views.User" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="Stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.10/themes/redmond/jquery-ui.css" /> 
       <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <label>Find Employee Code</label>
            <input type="text" id="txtEmployeeCode" class="form-control" placeholder="write some text to filter.." />
        </div>
    </div>
    <hr />
        <div class="row">
        <div class="col-md-12">
            <h4>Filter Result:</h4>
                <table class="table table-stripped" id="gridEmployees">
        <thead>
            <tr>
                <th>Employee Full Name</th>
                 <th>Employee Code</th>
                <th>Year</th>
                <th>Month</th>
                <th>Salary</th>
                <th>Employee Bonification</th>
            </tr>

        </thead>
        <tbody>

        </tbody>
    </table>
        </div>
    </div>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="../Scripts/Employee/employee.js"></script>
</asp:Content>
