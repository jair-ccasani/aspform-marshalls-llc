﻿using Prueba.Marshalls.BusinessLogic;
using Prueba.Marshalls.WebForm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Services;

namespace Prueba.Marshalls.WebForm.Views
{
    public partial class User : System.Web.UI.Page
    {
        


        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public  static List<string>GetEmployeeCodes(string employeeCode)
        {
            EmployeeBL  _logic = new EmployeeBL();

            List<string> EmployeeCodes =  _logic.FilterEmployeeCodeAsync(employeeCode);

            return EmployeeCodes;
        }
        [WebMethod]
        public  static List<EmployeeViewModel> GetEmployees(string employeeCode)
        {
            EmployeeBL _logic = new EmployeeBL();
            var employees =  _logic.ListEmployeeByCodeAsync(employeeCode);
            var bonoEmployee = employees.Take(3).Sum(x => x.Base_Salary ?? 0) / 3;

            var employeeVM = employees.Select(x => new EmployeeViewModel
            {
                Employee_Full_Name = $"{x.Employee_Name} {x.Employee_Surname}",
                Employee_Code = x.Employee_Code,
                Year = x.Year,
                Month = x.Month,
                Base_Salary = x.Base_Salary,
                Bono_Employee = bonoEmployee
            }).ToList();

            return employeeVM;
        }
    }
}