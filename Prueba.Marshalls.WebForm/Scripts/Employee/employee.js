﻿$(function () {



    $("#txtEmployeeCode").autocomplete({
        source: function (request, response) {
            fetch(
                "User.aspx/GetEmployeeCodes", {
                method: 'POST',
                    body: JSON.stringify({ employeeCode: request.term }),
                headers: { 'Content-Type': 'application/json' }
            }).then( response => {
                if (response.ok) { return response.json() }
                throw new Error(response.statusText);  // throw an error if there's something wrong with the response
            }).then(data => {
                //console.log(data.d);
                let dataResponse = [];
                if (data != null) {
                    dataResponse = [...data.d].map(item => ({ label: item, value:item}));
                }
                response(dataResponse);

            }).catch(error =>alert(error));
        },
        select: function (event, ui) {
            let item = ui.item.value;

            fetch(
                "User.aspx/GetEmployees", {
                method: 'POST',
                    body: JSON.stringify({ employeeCode: item }),
                headers: { 'Content-Type': 'application/json' }
            }).then(response => {
                if (response.ok) { return response.json() }
                throw new Error(response.statusText);  // throw an error if there's something wrong with the response
            }).then(data => {
                console.log(data.d);

                const element = CreateEmployeeTableBodyFromResult(data.d);
                $("#gridEmployees tbody").html(element);

            }).catch(error => alert(error));

        },
        change: function (event, ui) {
            if (ui.item === null) {

            }
        },
        messages: {
            noResults: "",
            results: function (count) {
                return count + (count > 1 ? ' results' : ' result ') + ' found';
            }
        }
    });


});

const CreateEmployeeTableBodyFromResult = (employees = []) => {
    let domString = "";
    const totalRow = employees.length;
    let i = 0;
    for (let employee of employees) {
        i++;

        domString += `
            <tr>
                <td>${employee.Employee_Full_Name}</td>
                <td>${employee.Employee_Code}</td>
                <td ${(i < 4 ? 'style="background-color:yellow;color:white;"' : '')}>${employee.Year}</td>
                <td ${(i < 4 ? 'style="background-color:yellow;color:white;"' : '')}>${employee.Month}</td>
                <td ${(i < 4 ? 'style="background-color:yellow;color:white;"' : '')}>${employee.Base_Salary}</td>
                ${(i == 1 ? `<td style="text-aling:center;" rowspan="${totalRow}">${employee.Bono_Employee.toFixed(2)}</td>` : '')}
                
            </tr>
        `;
    }

    return domString;
}