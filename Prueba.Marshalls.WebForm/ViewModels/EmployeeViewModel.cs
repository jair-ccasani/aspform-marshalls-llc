﻿using System;

namespace Prueba.Marshalls.WebForm.ViewModels
{
    public class EmployeeViewModel
    {
        
        public Nullable<int> Year { get; set; }
        public Nullable<int> Month { get; set; }
        public string Employee_Code { get; set; }
        public string Employee_Full_Name { get; set; }
        public Nullable<decimal> Base_Salary { get; set; }
        public Nullable<decimal> Bono_Employee { get; set; }


    }
}